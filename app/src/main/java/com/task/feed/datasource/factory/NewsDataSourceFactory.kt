package com.task.feed.datasource.factory

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.task.feed.datasource.NewsDataSource
import com.task.feed.model.data.Article
import com.task.feed.model.network.ApiClient
import io.reactivex.disposables.CompositeDisposable

class NewsDataSourceFactory(private val compositeDisposable: CompositeDisposable, private val apiClient: ApiClient) : DataSource.Factory<Int, Article>() {

    val newsDataSourceLiveData = MutableLiveData<NewsDataSource>()

    override fun create(): DataSource<Int, Article> {
        val newsDataSource = NewsDataSource(
            apiClient,
            compositeDisposable
        )
        newsDataSourceLiveData.postValue(newsDataSource)
        return newsDataSource
    }
}

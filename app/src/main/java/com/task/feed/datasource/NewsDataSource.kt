package com.task.feed.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.task.feed.State
import com.task.feed.model.data.Article
import com.task.feed.model.network.ApiClient
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class NewsDataSource(private val apiClient: ApiClient, private val compositeDisposable: CompositeDisposable) :
    PageKeyedDataSource<Int, Article>() {

    var state: MutableLiveData<State> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Article>) {
        updateState(State.LOADING)

        var page = 1

        compositeDisposable.add(
            apiClient.getTopHeadlines(page = page)
                .subscribe(
                    { response ->
                        updateState(State.DONE)
                        callback.onResult(
                            response.articles,
                            null,
                            page.inc()
                        )
                    },
                    {
                        updateState(State.ERROR)
                    }
                )
        )

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        updateState(State.LOADING)
        compositeDisposable.add(apiClient.getTopHeadlines(page = params.key).subscribe(
            { response ->
                updateState(State.DONE)
                callback.onResult(response.articles, params.key + 1)
            },
            {
                updateState(State.ERROR)
            }
        ))
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Article>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun updateState(state: State) {
        this.state.postValue(state)
    }
}

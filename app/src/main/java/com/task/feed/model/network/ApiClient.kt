package com.task.feed.model.network

import com.google.gson.GsonBuilder
import com.task.feed.Const
import com.task.feed.model.data.NewsPage
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

interface ApiClient {

    companion object {

        private lateinit var retrofit: Retrofit

        fun create(): ApiClient {
                retrofit = Retrofit.Builder().addConverterFactory(
                    GsonConverterFactory.create(
                        GsonBuilder().create())).addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Const.BASE_URL).build()
            return retrofit.create(ApiClient::class.java)
        }
    }

    @GET("/v2/top-headlines")
    fun getTopHeadlines(@Query("category") category: String = "Entertainment",
                        @Query("apiKey") apiKey: String = Const.API_KEY,
                        @Query("pageSize") pageSize: Int = Const.PAGE_SIZE,
                        @Query("page") page: Int
    ): Single<NewsPage>
}
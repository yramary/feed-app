package com.task.feed.model.data

import com.google.gson.annotations.SerializedName

data class NewsPage (
    @SerializedName("status") val status : String,
    @SerializedName("totalResults") val totalResults : Int,
    @SerializedName("articles") val articles : List<Article>
)
package com.task.feed.view.activity

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.task.feed.Const
import com.task.feed.model.data.Article
import com.task.feed.viewmodel.ContentViewModel
import com.test.myapplication.R
import kotlinx.android.synthetic.main.activity_news_content.*


class ContentActivity : AppCompatActivity() {

    private val viewModel = ContentViewModel()

    lateinit var article: Article
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_content)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        article = intent.getSerializableExtra(Const.ARTICLE_KEY) as Article
        viewModel.post(article)
        initObserver()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.itemId
        if (id == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initUI() {
        text_view_title.visibility = View.GONE
        text_view_content.visibility = View.GONE
        text_view_author.visibility = View.GONE
    }

    private fun initObserver() {
        viewModel.content.observe(this, Observer {
            it.title?.let { title ->
                text_view_title.text = title
                text_view_title.visibility = View.VISIBLE
            }
            it.content?.let { content ->
                text_view_content.text = content
                text_view_content.visibility = View.VISIBLE
            }
            it.author?.let { author ->
                text_view_author.text = author
                text_view_author.visibility = View.VISIBLE
            }
        })
    }
}
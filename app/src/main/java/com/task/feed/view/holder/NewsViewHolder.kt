package com.task.feed.view.holder

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.task.feed.Const
import com.task.feed.model.data.Article
import com.task.feed.view.activity.ContentActivity
import com.test.myapplication.R
import kotlinx.android.synthetic.main.item_news.view.*

class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(article: Article?) {
        if (article != null) {
            itemView.text_view_title.text = article.title
            itemView.text_view_date.text = formatTime(article.publishedAt)

            Glide.with(itemView).load(article.urlToImage).placeholder(R.drawable.ic_photo_24dp)
                .into(itemView.view_image)

            itemView.card_view_news.setOnClickListener { v ->
                val intent = Intent(itemView.context, ContentActivity::class.java)
                intent.putExtra(Const.ARTICLE_KEY, article)
                itemView.context.startActivity(intent)
            }
        }
    }

    private fun formatTime(time: String?): String? {
        return time?.replace("T", " ")?.replace("Z", " ")
    }

    companion object {
        fun create(parent: ViewGroup): NewsViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_news, parent, false)
            return NewsViewHolder(view)
        }
    }
}

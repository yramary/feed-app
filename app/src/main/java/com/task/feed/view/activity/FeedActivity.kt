package com.task.feed.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.task.feed.State
import com.task.feed.view.adapter.NewsListAdapter
import com.task.feed.viewmodel.FeedViewModel
import com.test.myapplication.R
import kotlinx.android.synthetic.main.activity_feed.*
import kotlinx.android.synthetic.main.activity_feed.progress_bar

class FeedActivity : AppCompatActivity() {

    private lateinit var viewModel: FeedViewModel
    private lateinit var newsListAdapter: NewsListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feed)

        viewModel = ViewModelProviders.of(this)
            .get(FeedViewModel::class.java)

        initAdapter()
        initState()

        layout_refresh.setOnRefreshListener {
            refreshContent()
        }
    }

    private fun refreshContent() {
        layout_refresh.isRefreshing = true
        viewModel.invalidate()
        layout_refresh.isRefreshing = false
    }

    private fun initAdapter() {
        newsListAdapter = NewsListAdapter()
        recycler_view_news.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        (recycler_view_news.layoutManager as LinearLayoutManager).stackFromEnd = false
        recycler_view_news.adapter = newsListAdapter
        viewModel.newsList.observe(this, Observer {
            newsListAdapter.submitList(it)
        })
    }

    private fun initState() {
        viewModel.getState().observe(this, Observer { state ->
            progress_bar.visibility =
                if (viewModel.listIsEmpty() && state == State.LOADING)
                    View.VISIBLE
                else
                    View.GONE
            if (!viewModel.listIsEmpty()) {
                newsListAdapter.setState(state ?: State.DONE)
            }
        })
    }
}

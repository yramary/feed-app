package com.task.feed

object Const {
    const val BASE_URL = "https://newsapi.org/"
    const val API_KEY = "a984065f40434476bbdf54c45eebba7c"
    const val ARTICLE_KEY = "Article"
    const val PAGE_SIZE = 10
    const val SOURCE_PAGE = 1
}
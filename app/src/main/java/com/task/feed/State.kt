package com.task.feed

enum class State {
    DONE, LOADING, ERROR
}

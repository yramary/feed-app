package com.task.feed.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.task.feed.model.data.Article

class ContentViewModel() : ViewModel() {
    var content = MutableLiveData<Article>()

    fun post(article: Article) {
        content.postValue(article)
    }

}
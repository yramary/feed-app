package com.task.feed.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.task.feed.Const
import com.task.feed.State
import com.task.feed.datasource.NewsDataSource
import com.task.feed.datasource.factory.NewsDataSourceFactory
import com.task.feed.model.data.Article
import com.task.feed.model.network.ApiClient
import io.reactivex.disposables.CompositeDisposable

class FeedViewModel : ViewModel() {

    private val apiClient: ApiClient = ApiClient.create()
    var newsList: LiveData<PagedList<Article>>
    private val compositeDisposable = CompositeDisposable()
    private val pageSize = Const.PAGE_SIZE
    private val newsDataSourceFactory: NewsDataSourceFactory

    init {
        newsDataSourceFactory = NewsDataSourceFactory(compositeDisposable, apiClient)
        val config = PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(pageSize)
            .setEnablePlaceholders(false)
            .build()
        newsList = LivePagedListBuilder<Int, Article>(newsDataSourceFactory, config).build()
    }


    fun getState(): LiveData<State> = Transformations.switchMap<NewsDataSource,
            State>(newsDataSourceFactory.newsDataSourceLiveData, NewsDataSource::state)

    fun invalidate() {
        newsDataSourceFactory.newsDataSourceLiveData.value?.invalidate()
    }

    fun listIsEmpty(): Boolean {
        return newsList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}
